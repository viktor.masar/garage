SHELL := cmd.exe

export AS_BUILD_MODE := Rebuild
export AS_SYSTEM_PATH := C:/BRAutomation/AS/System
export AS_BIN_PATH := C:/BRAutomation/AS412/bin-en
export AS_INSTALL_PATH := C:/BRAutomation/AS412
export AS_PATH := C:/BRAutomation/AS412
export AS_VC_PATH := C:/BRAutomation/AS412/AS/VC
export AS_GNU_INST_PATH := C:/BRAutomation/AS412/AS/gnuinst/V4.1.2
export AS_STATIC_ARCHIVES_PATH := C:/projects/Moje/Garage/Temp/Archives/Config2/X20CP1381_RT
export AS_CPU_PATH := C:/projects/Moje/Garage/Temp/Objects/Config2/X20CP1381_RT
export AS_CPU_PATH_2 := C:/projects/Moje/Garage/Temp/Objects/Config2/X20CP1381_RT
export AS_TEMP_PATH := C:/projects/Moje/Garage/Temp
export AS_BINARIES_PATH := C:/projects/Moje/Garage/Binaries
export AS_PROJECT_CPU_PATH := C:/projects/Moje/Garage/Physical/Config2/X20CP1381_RT
export AS_PROJECT_CONFIG_PATH := C:/projects/Moje/Garage/Physical/Config2
export AS_PROJECT_PATH := C:/projects/Moje/Garage
export AS_PROJECT_NAME := Garage
export AS_PLC := X20CP1381_RT
export AS_TEMP_PLC := X20CP1381_RT
export AS_USER_NAME := masarv
export AS_CONFIGURATION := Config2
export AS_COMPANY_NAME := B&R\ Industrial\ Automation\ GmbH
export AS_VERSION := 4.12.3.127\ SP
export AS_WORKINGVERSION := 4.12


default: \



